{  pkgs, ... }:
{
  services.taffybar = {
    enable = true;
    # package = pkgs.haskellPackages.custom-taffybar;
                      };
  services.status-notifier-watcher.enable = true;
  # home.file.".config/taffybar/taffybar.css".source = ./taffybar.css;
  # home.file.".config/taffybar/gotham.css".source = ./gotham.css;
  # home.file.".config/taffybar/taffybar.hs".source = ./taffybar.hs;
}
