(add-to-list 'load-path "~/nixos-m1/emacs")

(load-theme 'modus-vivendi t)

(setq org-agenda-files '("~/tasks/timetable.org")
      org-agenda-span  'week
      org-todo-keywords '((sequence "NEXT(n)" "TODO(t)" "WAITING(w)" "SOMEDAY(s)" "|" "DONE(d)" "CANCELLED (c)")) 
      org-log-done 'time
      org-log-done 'note
      )
(global-set-key (kbd "C-c a") 'org-agenda)

(setq backup-directory-alist `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))

(make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)
(setq auto-save-list-file-prefix (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
      auto-save-file-name-transforms `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))

(setq create-lockfiles nil)

(setq org-ellipsis "▼  " ;;⤵ 
	;; org-startup-indented t
	org-src-tab-acts-natively t
	org-hide-emphasis-markers t
	org-fontify-done-headline t
	org-hide-leading-stars t
	org-pretty-entities t
	org-odd-levels-only t
	) 
  ;; (add-hook 'org-mode-hook #'org-shifttab)
  ;; (add-hook 'org-mode-hook #'variable-pitch-mode)
  ;; (setq org-bullets-bullet-list '(
  ;; (add-hook 'org-mode-hook (lambda () (local-set-key (kbd "
(setq org-src-fontify-natively t
org-startup-folded t
	org-edit-src-content-indentation 0)

(defun my-org-confirm-babel-evaluate (lang body)
  (not (string= lang "haskell"))) ;; don't ask for haskell
(setq org-confirm-babel-evaluate #'my-org-confirm-babel-evaluate)
(setq org-confirm-babel-evaluate 'nil)

(setq org-babel-load-languages '((emacs-lisp . t) (shell . t) (dot . t) (haskell  . t)))

(defun insert-org-haskell ()
    "Inserts code haskell block in org"
    (interactive)
    (insert "#+BEGIN_SRC haskell")
    (newline)
    (insert ":{")
    (newline)
    (insert ":}")
    (newline)
    (insert "#+END_SRC ")
    (previous-line 2)
    (newline)
)

(defun reload ()
  (interactive )
  (load-file (expand-file-name "~/.emacs.d/init.el")))

(defun private-config ()
  (interactive )
  (find-file (expand-file-name "~/nixos-m1/emacs/config.org")))
;; (require 'emux-minibuffer)

(require 'dired)

(put 'dired-find-alternate-file 'disabled nil)
;; Old alternative for dired-kill-when-opening-new-dired-buffer option.
(add-hook 'dired-mode-hook 'dired-hide-details-mode)
(add-hook 'dired-mode-hook 'dired-omit-mode)
(setq-default dired-recursive-copies 'top   ;; Always ask recursive copy
	      dired-recursive-deletes 'top  ;; Always ask recursive delete
	      dired-dwim-target t	    ;; Copy in split mode with p
	      dired-auto-revert-buffer t
	      dired-listing-switches "-alh -agho --group-directories-first"
	      dired-kill-when-opening-new-dired-buffer t ;; only works for emacs > 28
	      dired-isearch-filenames 'dwim ;;)
	      dired-omit-files "^\\.[^.].*"
	      dired-omit-verbose nil
	      dired-hide-details-hide-symlink-targets nil
	      delete-by-moving-to-trash t
	      )
;; (emux/leader-key-def 
;; "fd" 'dired-jump)

;; Save history
(savehist-mode t)
;; saveplace
(save-place-mode t)                           ;; Remember point in files
(setq save-place-ignore-files-regexp  ;; Modified to add /tmp/* files
      (replace-regexp-in-string "\\\\)\\$" "\\|^/tmp/.+\\)$"
				save-place-ignore-files-regexp t t))
(recentf-mode t)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)

(define-key minibuffer-local-must-match-map (kbd "<escape>") 'abort-minibuffers)
  (require 'prog-keybindings)
  (require 'org-keybindings)
  (require 'dired-keybindings)

(add-hook 'Info-mode-hook 
	  (lambda ()
	    (define-key Info-mode-map "j" 'scroll-up-line)
	    (define-key Info-mode-map "k" 'scroll-down-line)
	    ))

;;#'variable-pitch-mode)

(global-set-key (kbd "C-c b") 'emux/get-and-to-window)
(global-set-key (kbd "C-c c b") 'emux/close-window)

(global-set-key "\C-x\C-m" 'execute-extended-command)
(global-set-key [remap dabbrev-expand] #'hippie-expand)

(global-set-key (kbd "C-z" ) #'(lambda (nil) (message "pressed C-z")))

(global-set-key (kbd "M-." ) #'(lambda () (message "pressed C-z")))

(add-hook 'help-mode-hook 
	  (lambda ()
	    (define-key help-mode-map "j" 'scroll-up-line)
	    (define-key help-mode-map "k" 'scroll-down-line)
	    ))

(global-set-key (kbd "C-x C-o" ) 'other-window)

(defun emux/window-front ()
  (interactive)
  (other-window 1))

(defun emux/window-back ()
  (interactive)
  (other-window -1))

(defun emux/horizontal-split ()
  (interactive )
  (split-window-below)
  (emux/window-front))
;; (other-window 1))

(defun emux/vertical-split ()
  (interactive )
  (split-window-right)
  (emux/window-front))
;;  (other-window 1))

(defun emux/up-window ()
  (interactive )
  (emux/window-back))
;;  (other-window -1))

(defun emux/down-window ()
  (interactive )
  (emux/window-front))
(defun emux/swap-with-front-window ()
  (interactive)
  (save-excursion
    (let ((cur-buff (current-buffer)))
      (emux/window-front)
      (let ((other-buff (current-buffer)))
	(switch-to-buffer cur-buff)
	(emux/window-back)
	(switch-to-buffer other-buff)))))
(defun emux/swap-with-back-window ()
  (interactive)
  (save-excursion
    (let ((cur-buff (current-buffer)))
      (emux/window-back)
      (let ((other-buff (current-buffer)))
	(switch-to-buffer cur-buff)
	(emux/window-front)
	(switch-to-buffer other-buff)))))
;;	   (other-window 1))

(defun emux/getBuffer()
  (interactive)
  (setq buff (read-buffer-to-switch "Raise window"))
  (display-buffer buff '(display-buffer-reuse-window . ())))

(defun emux/go-to-window ()
  (interactive)
  (setq buff (read-buffer-to-switch "Go to window"))
  (select-window (get-buffer-window buff)))

(defun emux/get-and-to-window ()
  (interactive)
  (setq buff (read-buffer-to-switch "Raise & Go to window"))
  (display-buffer buff '(display-buffer-reuse-window . ()))
  (select-window (get-buffer-window buff)))

(defun emux/close-window ()
  (interactive)
  (setq buff (read-buffer-to-switch "Close window"))
  (delete-window (get-buffer-window buff)))

(defun emux/close-help ()
  (interactive)
  (setq buff "*Help*") 
  (delete-window (get-buffer-window buff)))

(defun emux/close-scratch ()
  (interactive)
  (setq buff "*scratch*") 
  (delete-window (get-buffer-window buff)))

(require 'window)
(setq display-buffer-alist
      `(
	("\\*vterm*"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . bottom)
	 (slot . 0)
	 )
	("\\*shell*"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . bottom)
	 (slot . -1)
	 )
	("\\`\\*Async Shell Command\\*\\'"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . right)
	 (slot . 0)
	 )
	("\\*scratch*"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . top)
	 (slot . 0)
	 )
	;; ("*"
	;;  (display-buffer-in-side-window)
	;;  (mode . (dired-mode))
	;;  (dedicated . t)
	;;  (window-height . 0.23)
	;;  (side . bottom)
	;;  (slot . 0)
	;;  )
	("*Help*"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . bottom) 
	 (slot . 1)
	 )
	("*Occur*"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . top) 
	 (slot . -1)
	 )
	("*grep*"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . top) 
	 (slot . 1)
	 )
	("\\*haskell*"
	 (display-buffer-in-side-window)
	 (dedicated . t)
	 (window-height . 0.23)
	 (side . bottom)
	 (slot . -1)
	 )
	)
      )
;;(setq display-buffer-base-action 
					; (custom-set-variables 
;;  '(display-buffer-base-action '((display-buffer-reuse-window display-buffer-same-window))))
;; (setq display-buffer-base-action '((display-buffer-reuse-window display-buffer-same-window)))

(column-number-mode 1)
(global-display-line-numbers-mode 0)
(setq display-line-numbers-type 'visual)
(dolist (mode '( org-mode-hook
		 prog-mode-hook
		 dired-mode-hook
		 ;; term-mode-hook
		 ;; vterm-mode-hook
		 ;; shell-mode-hook
		 ;; dired-mode-hook
		 ;; eshell-mode-hook
		 ))
  (add-hook mode (lambda () (display-line-numbers-mode 1))))

(electric-pair-mode t)

(setq fontaine-presets
            '((regular
               :default-height 100)
              (small
               :default-height 50)
              (large
               :default-weight semilight
               :default-height 120
               :bold-weight extrabold)
              (extra-large
               :default-weight semilight
               :default-height 150
               :bold-weight extrabold)
              (t                        ; our shared fallback properties
               :default-family "Fira Code"
               :default-weight normal)))
      (fontaine-set-preset 'regular)

(require 'ob-haskell)
    (add-hook 'haskell-mode-hook  'interactive-haskell-mode)
    (setq  haskell-interactive-popup-errors nil)

(envrc-global-mode)

(require 'pdf-tools)
