{config,pkgs,...}:{
 programs.emacs = {
   enable = true;
   package = pkgs.emacs; # pkgs.emacsNativeComp; # pkgs.emacsPgtkGcc;
      extraPackages = epkgs:
      with epkgs; [
        org
        vterm
        # flycheck
        # dante
        org-journal
fontaine
#
nix-mode
haskell-mode
envrc
magit
        pdf-tools
	];
 };
  services.emacs = {
    enable = true;
    client = {
      enable = true;
      arguments = [ "-c" ];
    };
    defaultEditor = true;
    socketActivation.enable = true;
  };
   home.file.".emacs.d/init.el".source = ./init.el;
    home.file.".emacs.d/early-init.el".source = ./early-init.el;
#  home.packages = with pkgs; [ emacs-all-the-icons-fonts ripgrep fd ];
}
