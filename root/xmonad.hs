import XMonad 
import qualified XMonad.StackSet as W
import qualified XMonad as X

import System.Exit

import Data.Maybe (fromJust,isJust)
import qualified Data.Map as M

import XMonad.Actions.Commands
import XMonad.Actions.CycleWS
import XMonad.Actions.DynamicProjects
import XMonad.Actions.GridSelect
import XMonad.Actions.MouseResize
import XMonad.Actions.Search
import XMonad.Actions.Submap
import XMonad.Actions.WindowGo

-- hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks (avoidStruts,docks, docksEventHook, manageDocks, ToggleStruts(..))
import XMonad.Hooks.SetWMName
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP

-- Layouts
import XMonad.Layout.Accordion
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns

-- Layouts modifiers
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.Magnifier
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import XMonad.Layout.WindowNavigation
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

import XMonad.Util.Dmenu
import XMonad.Util.EZConfig
import XMonad.Util.NamedScratchpad
import XMonad.Util.NamedWindows (getName)
import XMonad.Util.Run
import XMonad.Util.SpawnOnce
import XMonad.Util.Ungrab

--prompts
import XMonad.Prompt
import XMonad.Prompt.AppLauncher as AL
import XMonad.Prompt.ConfirmPrompt
-- import XMonad.Prompt.OrgMode
import XMonad.Prompt.Shell
import XMonad.Prompt.Window
import XMonad.Prompt.XMonad
import XMonad.Prompt.AppendFile


import Data.List.Split

myLauncher :: String
myLauncher = "dmenu_run -n -fn \"Cousine:bold:size=34\""

dswitcher :: String
dswitcher = "~/aios/dmenu/windowSwitcher.sh"

xmenu :: String
xmenu = "~/aios/xmenu/xmenu.sh"

myWorkspaces :: [String]
myWorkspaces = ["λ=",">>=", "<*>","<$>","<>"]
myWorkspaceIndices = M.fromList $ zip myWorkspaces [1..] -- (,) == \x y -> (x,y)

myMainKey :: String
myMainKey = "M-"

myEditor :: String
myEditor = "emacs" -- "emacsclient -c -a emacs"
-- myTerminal      = "kitty" --"alacritty"

myTerminal :: String
myTerminal = "alacritty"

myBrowser :: String
myBrowser =  "firefox" --""

-- my multi media browser
myMulBrowser :: String
myMulBrowser =  "firefox" --""

myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

myClickJustFocuses :: Bool
myClickJustFocuses = False

myBorderWidth :: Dimension
myBorderWidth   = 0

myNormColor :: String       -- Border color of normal windows
myNormColor   = colorBack   -- This variable is imported from Colors.THEME

myFocusColor :: String      -- Border color of focused windows
myFocusColor  = color15     -- This variable is imported from Colors.THEME


-- myNormColor :: String
-- myNormColor   = "#282c34"   -- Border color of normal windows

-- myFocusColor :: String
-- myFocusColor  = "#ffffff"   -- Border color of focused windows

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length .  W.integrate' . W.stack . W.workspace . W.current . windowset

myModMask       = mod4Mask

color01 = "#1c1f24"
color02 = "#ff6c6b"
color03 = "#98be65"
color04 = "#da8548"
color05 = "#51afef"
color06 = "#c678dd"
color07 = "#5699af"
color08 = "#202328"
color09 = "#5b6268"
color10 = "#da8548"
color11 = "#4db5bd"
color12 = "#ecbe7b"
color13 = "#3071db"
color14 = "#a9a1e1"
color15 = "#46d9ff"
color16 = "#dfdfdf"
colorBack = "#282c34"
colorFore = "#bbc2cf"

myFont :: String
myFont = "xft:Cousine:regular:size=19:antialias=true:hinting=true"

-- not using now
myDmenu = menuArgs "dmenu" ["-l","10","-n","-fn","Lucida MAC:size=40"]

ezKeys :: [(String,String,X())]
ezKeys = [
  -- ("`","prev non empty",moveTo Prev nonEmptyNonNSP)
  ("a b" , "bluetooth", spawn $ "pavucontrol ;" <> myTerminal <> " -e bluetoothctl")
    -- spawn $ myBrowser ++ " https://rycee.gitlab.io/home-manager/options.html" )
  -- , ("a","weblist",makeGrid2 appList)
  ,("c f","nixos flake",spawn $ myEditor <> " ~/aios/flake.nix" )
  ,("c x","nixos xmonad",spawn $ myEditor <> " ~/aios/desktop/config.hs" )
  ,("c e","nixos emacs",spawn $ myEditor <> " ~/aios/editor/emacs/emacs.org" )
  ,("c p","nixos genPackages",spawn $ myEditor <> " ~/aios/genPackages/default.nix" )
  ,("e d","dired emacs",spawn $ "emacsclient -c -n -e '(dired nil)'" )
  ,("e e","recent emacs",spawn $ "emacsclient -c -n -e '(recentf-open-files)'" )
  ,("e r","restart emacs",spawn $ "systemctl --user restart emacs.service" )
  , ("f", "fullScreen",sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full
  ,("g" ,"google",googlePrompt )--spawn "~/vc/firefox/google.sh")
  -- KB_GROUP Window resizing
        -- , ("h", "shrink",sendMessage Shrink)                   -- Shrink horiz window width
  ,("h", "test" ,makeGrid2 testList)
  ,("i", "hdmi" , makeGrid2 hdmiList)
  , ("d" ,"date", myGround)
  -- ,("j" , "down" , 
  -- ,("j","windows focus down",do
  --      res <- windowCount
  --      if res == Just "1" || res == Just "0"
  --        then moveTo Next nonEmptyNonNSP
  --        else windows W.focusDown)
  -- ,("k","windows focus up",do
  --      res <- windowCount
  --      if res == Just "1" || res == Just "0"
  --        then moveTo Prev nonEmptyNonNSP
  --        else windows W.focusUp)
  , ("l", "expand", sendMessage Expand)                   -- Expand horiz window width
  ,("m", "focus master",windows W.focusMaster)
  ,("n u","nixos update",  spawn "nix run \"/home/vamshi/aios#cosmos\" --impure  && notify-send \"Successfully updated\" || notify-send -u critical \"Updating failed!!\" ")
  ,("n s","search", nixosPrompt) -- spawn $ "~/aios/dmenu/nixos")
  ,("n h" , "home manager", spawn $ myBrowser ++ " https://rycee.gitlab.io/home-manager/options.html" )
  ,("o","window Switcher",alttab)--windowPrompt windowXPConfig Goto windowMap')
  ,("p p","playerctl",spawn "playerctl play-pause")--makeGrid2 playerCtlList)
  ,("q","shellprompt",shellPrompt shellXPConfig)
  ,("r","redshift",  makeGrid2 redList)
  ,("t","force tiling", withFocused $ windows . W.sink)
  ,("u","add workspaces",switchProjectPrompt googleXPConfig  )
  ,("x M-f","project find file",spawn $ "emacsclient -c -n -e '(project-find-file)'")
  ,("y" ,"youtube", youtubePrompt)--spawn "~/vc/firefox/google.sh")
  ,("S-e","exit",  makeGrid2 exitList)
  ,("S-f", "fullScreen", sendMessage ToggleStruts) -- Toggles noborder/full
  ,("S-m", "swapmaster", windows W.swapMaster) -- Swap the focused window and the master window
  ,("S-q","xmonad --logout",io (exitWith ExitSuccess))
  ,("S-w","windows focus up",windows W.focusUp)
  ,("S-[", "shiftToPrev ",shiftTo Prev nonNSP )
  ,("S-]","shiftToNext ", shiftTo Next nonNSP )
  ,("w","windows focus down",do
       res <- windowCount
       if res == Just "1" || res == Just "0"
         then moveTo Next nonEmptyNonNSP
         else windows W.focusDown)
  ,("<Backspace>","kill", mbackspace)
 ,("<Return>", "terminal",namedScratchpadAction myScratchPads "terminal")
  , ("<Tab>", "nextLayout", sendMessage NextLayout)           -- Switch to next layout
  -- , ("S-<Tab>", "nextLayout", sendMessage PrevLayout)           -- Switch to next layout
  ,("[","previous workspace",moveTo Prev nonNSP)
  ,("]","next workspace",moveTo Next nonNSP)
  ,("1","editor emacs", raiseNextMaybe  (confirmPrompt windowXPConfig "Emacs" $ spawn myEditor)(className =? "Emacs"))
  ,("2" , "browser", raiseNextMaybe (xmonadPromptC webList keyXPConfig ) ((className =? "firefox") ))
  ,("3","zathura", raiseNextMaybe ( confirmPrompt windowXPConfig "zathura" $ spawn "zathura" ) (className =? "Zathura"))
  ,("4","spotify",raiseNextMaybe (confirmPrompt googleXPConfig "spotify" $ spawn "spotify")(className =? "Spotify") )
  ,("5","kitty",raiseNextMaybe (confirmPrompt googleXPConfig "kitty" $ spawn "kitty")(className =? "kitty") )
  ,("`","desktop",raiseNextMaybe  (confirmPrompt windowXPConfig "Emacs" $ spawn myEditor)(className =? "Emacs") )]
  where nonNSP          = WSIs (return (\ws -> W.tag ws /= "NSP"))
        nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "NSP"))

miscKeys :: [(String,String,X())]
miscKeys = [--("C-<Space>" ,"" ,spawn dswitcher)
           ("M1-<Space>", "infinte recursion",runCommandConfig  myDmenu . skip1element $ ezKeys <> miscKeys )
          , ("M1--","sound decrease",spawn "amixer set Master 10%- unmute")
          -- , ("","terminal",spawn "alacritty")
          ,("M1-<Return>", "terminal",namedScratchpadAction myScratchPads "terminal")
          , ("M1-=","sound decrease",spawn "amixer set Master 10%+ unmute")   
  , ("<XF86AudioPrev>",  "mocp next"           , spawn "playerctl previous")
  , ("<XF86AudioNext>",  "mocp prev"           , spawn "playerctl next")
  , ("<XF86AudioPlay>",  "mocp play"           , spawn "playerctl play-pause")
  , ("<XF86AudioMute>",  "Toggle audio mute"   , spawn "amixer set Master toggle")
  , ("<XF86AudioLowerVolume>",  "Lower vol"    , spawn "amixer set Master 5%- unmute")
  , ("<XF86AudioRaiseVolume>", "Raise vol"    , spawn "amixer set Master 5%+ unmute")
        ]

skip2element xs = [(p,r)| (p,q,r) <- xs]

skip1element xs = [(q,r)| (p,q,r) <- xs]

mainTurner :: [a] -> [([a], b)] -> [([a], b)]
mainTurner key xs = [(key <> p,q)|(p,q) <- xs]


myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

vamKeys =   mainTurner myMainKey (skip2element ezKeys) <> skip2element miscKeys

makeGrid2 = runSelectedAction (gsconfig2 myColorizer)


gsconfig2 colorizer =  (buildDefaultGSConfig colorizer){ gs_cellheight = 100,
                                                         gs_cellwidth = 200 ,
                                                         gs_navigate = mynavNSearch,
                                                         gs_font = myFont }   -- "xft:Consolas:size=23"}

mynavNSearch = makeXEventhandler $ shadowWithKeymap navNSearchKeyMap navNSearchDefaultHandler
  where navNSearchKeyMap = M.fromList [
          ((0,xK_Escape) , cancel)
          ,((0,xK_space)     , select)
          ,((controlMask,xK_e)     , select)
          ,((0,xK_Return)     , select)
          ,((0,xK_Left)       , move (-1,0) >> mynavNSearch)
          ,((0,xK_Right)      , move (1,0) >> mynavNSearch)
          ,((0,xK_Down)       , move (0,1) >> mynavNSearch)
          ,((0,xK_Up)         , move (0,-1) >> mynavNSearch)
          ,((0,xK_Tab)        , moveNext >> mynavNSearch)
          ,((shiftMask,xK_Tab), movePrev >> mynavNSearch)
          ,((0,xK_BackSpace), transformSearchString (\s -> if s == "" then "" else init s) >> navNSearch)
          ]
        navNSearchDefaultHandler (_,s,_) = do
          transformSearchString (++ s)
          mynavNSearch

myNavigation :: TwoD a (Maybe a)
myNavigation = makeXEventhandler $ shadowWithKeymap navKeyMap navDefaultHandler
 where navKeyMap = M.fromList [
          ((0,xK_Escape), cancel)
         ,((0,xK_Return), select)
         ,((0,xK_slash) , substringSearch navNSearch)--myNavigation)
         ,((0,xK_Left)  , move (-1,0)  >> myNavigation)
         ,((0,xK_h)     , move (-1,0)  >> myNavigation)
         ,((0,xK_Right) , move (1,0)   >> myNavigation)
         ,((0,xK_l)     , move (1,0)   >> myNavigation)
         ,((0,xK_Down)  , move (0,1)   >> myNavigation)
         ,((0,xK_j)     , move (0,1)   >> myNavigation)
         ,((0,xK_Up)    , move (0,-1)  >> myNavigation)
         ,((0,xK_y)     , move (-1,-1) >> myNavigation)
         ,((0,xK_Tab)     , moveNext     >> myNavigation)
         ,((mod4Mask,xK_w), moveNext     >> myNavigation)
         ,((mod4Mask,xK_e), select)
         ,((mod4Mask,xK_q), select)
         ,((0,xK_e), select)
         ,((0,xK_i)     , move (1,-1)  >> myNavigation)
         ,((0,xK_n)     , move (-1,1)  >> myNavigation)
         ,((0,xK_m)     , move (1,-1)  >> myNavigation)
         -- ,((mod4Mask,xK_w) , select)--setPos (0,0) >> myNavigation)
         ,((0,xK_space) , select)--setPos (0,0) >> myNavigation)
         ,((0,xK_s) , select)--setPos (0,0) >> myNavigation)
         ]
       navDefaultHandler = const myNavigation

alttab = goToSelected def {gs_cellheight = 100,gs_cellwidth=390,
                           gs_font= myFont,   --   "xft:Consolas:size=20",
                           gs_navigate = myNavigation,gs_colorizer = myColorizer'}

myColorizer' :: a -> Bool -> X (String,String)
myColorizer' _ True = return ("#46eed9","#000000")
myColorizer' _ False = return ("#000000","#dddddd")

myColorizer :: a -> Bool -> X (String,String)
myColorizer _ True = return ("#46d9ee","#000000")
myColorizer _ False = return ("#000000","#dddddd")

googlePrompt :: X ()
googlePrompt = promptSearchBrowser googleXPConfig myBrowser google

nixosSearch = searchEngine "nixos"        "https://search.nixos.org/packages?channel=unstable&from=0&size=50&sort=relevance&type=packages&query="
nixosPrompt = promptSearchBrowser googleXPConfig myBrowser nixosSearch

youtubePrompt :: X ()
youtubePrompt = promptSearchBrowser youtubeXPConfig myMulBrowser youtube


shellXPConfig :: XPConfig
shellXPConfig = greenXPConfig {
  -- autoComplete      = Nothing--Just 100000    -- set Just 100000 for .1 sec
   height            = 70
  ,promptKeymap = emacsLikeXPKeymap
  , showCompletionOnTab = False
  ,  position = Top
      --CenteredAt {xpCenterY = 0.19 , xpWidth = 0.88}
   ,  font = myFont  -- "xft:Consolas:size=20"
  ,defaultPrompter = const "λ= "
  ,fgColor = myFocusColor
  }

windowXPConfig :: XPConfig
windowXPConfig = greenXPConfig {
   font = myFont  -- "xft:Consolas:size=20"
   , height            = 70
   , autoComplete      = Just 100000    -- set Just 100000 for .1 sec
   ,  position = CenteredAt {
       xpCenterY = 0.19 , xpWidth = 0.88
       }
                               }
emacsXPConfig :: XPConfig
emacsXPConfig = shellXPConfig {
  defaultPrompter = const "Emacs: "
                    -- ,defaultText = "emacsclient -c -a emacs "
  }

zathuraXPConfig :: XPConfig
zathuraXPConfig = shellXPConfig {
  defaultPrompter = const "Zathura: "
  }

keyXPConfig :: XPConfig
keyXPConfig = googleXPConfig {
  autoComplete = Just 100000
  , fgColor = myFocusColor
  , borderColor = myFocusColor
  ,defaultPrompter =  const "firefox: "
  }

googleXPConfig :: XPConfig
googleXPConfig = greenXPConfig {
  fgColor = "white"--myFocusColor
  ,  position =  CenteredAt {xpCenterY = 0.29 , xpWidth = 0.48}
  ,  font = myFont  -- "xft:Consolas:size=22"
  , height            = 80
  }
youtubeXPConfig :: XPConfig
youtubeXPConfig = googleXPConfig {
  fgColor = "red"--myFocusColor
  ,bgColor = "white"--myFocusColor
  }

exitXPConfig :: XPConfig
exitXPConfig = googleXPConfig {
  fgColor = "red"--myFocusColor
  ,bgColor = "black"--myFocusColor
  }

appList :: [(String,X ())]
appList = [("vamshi5070" , spawn $ myBrowser <> " https://gitlab.com/vamshi5070/")
          , ("dt", spawn $ myBrowser <> " https://gitlab.com/dwt1/dotfiles/")
          , ("alonzoAlan", spawn $ myBrowser <> " https://github.com/alonzoAlan")
          , ("bluetooth",spawn $ "pavucontrol ;" <> myTerminal <> " -e bluetoothctl")
         ,("homeManager",spawn $ myBrowser ++ " https://rycee.gitlab.io/home-manager/options.html")
         ,("whatsapp",spawn $ myBrowser <> " web.whatsapp.com")
         ,("search", spawn $ "~/aios/dmenu/nixos")
        , ("M-M1-j", sendMessage MirrorShrink)          -- Shrink vert window width
        , ("M-M1-k", sendMessage MirrorExpand)          -- Expand vert window width
          ]

webList ::  [(String,X())]
webList = [
  ("google" , googlePrompt)--spawn "~/vc/firefox/google.sh")
  ,("youtube" , youtubePrompt)--spawn "~/vc/firefox/google.sh")
          ]

playerCtlList :: [(String,X())]
playerCtlList = [
  ("play-pause" , spawn "playerctl play-pause")
  ,("next" , spawn "playerctl next")
  ,("prev" , spawn "playerctl prev")
                ]

exitList :: [(String,X())]
exitList = [
  ("shutdown",do
      notePoweroff
      confirmPrompt exitXPConfig "Shutdown" $ spawn "poweroff"
        )
  ,("reboot",confirmPrompt exitXPConfig "Reboot" $ spawn "reboot")
  ]

database = "/home/vamshi/nixos-m1/database.txt" 

notePoweroff :: X ()
notePoweroff = do
  -- spawn $ "date  '+%H-%M-%d-%m-%Y' > /home/vamshi/nixos-m1/database.txt"
  time <- liftIO $ runProcessWithInput "bash" []  "date +'%H-%M-%d-%m-%Y'"
  liftIO $ writeFile database time
-- previousLogin = do
testList = [("data", notePoweroff )]

retriveData = do
  previoustime <-  readFile database
  currentTime <-  runProcessWithInput "bash" []  "date +'%H-%M-%d-%m-%Y'"
  return $  convertToString $ difference currentTime previoustime
  
redList :: [(String,X())]
redList = [
  ("restart",spawn "systemctl --user restart redshift.service || systemctl --user restart gammastep.service")
  ,("urestart",spawn "systemctl --user restart redshift.service || systemctl --user restart gammastep.service ")
  ,("stop",spawn "systemctl --user stop redshift.service || systemctl --user stop gammastep.service")
  ,("open",spawn $  myTerminal <> " -e sudo kak /etc/nixos/redshift/default.nix")
    ]

hdmiList :: [(String,X())]
hdmiList = [
  ("hdmi",spawn  "xrandr --output eDP-1 --off ; xrandr --output HDMI-1 --auto")
  ,("edp",spawn  "xrandr --output HDMI-1 --off ; xrandr --output eDP-1 --auto ")
    ]

-- | A map from window names to Windows, given a windowTitler function.
windowMap'' :: (X.WindowSpace -> Window -> Int -> X String) -> X (M.Map String Window)
windowMap'' titler = do
  ws <- gets X.windowset
  M.fromList . concat <$> mapM keyValuePairs (W.workspaces ws)
  where keyValuePairs ws = mapM (keyValuePair ws) $ zip (W.integrate' (W.stack ws)) [0..]
        keyValuePair ws (w,num) = flip (,) w <$> titler ws w num

decorateName :: X.WindowSpace -> Window -> Int -> X String
decorateName ws w num = do
  name <- show <$> getName w
  return $ (show num) <> " " <> name <> " [" ++ W.tag ws ++ "]"

-- | A map from window names to Windows.
windowMap' :: X (M.Map String Window)
windowMap' = windowMap'' decorateName

mbackspace = do
       res <- windowCount
       len <- allWindows
       if  (M.size len) == 0
         then makeGrid2 exitList
         else if res == Just "0"
              then moveTo Next NonEmptyWS -- makeGrid2 exitList
              else kill


myGround :: X ()
myGround = do
  -- bat <- runProcessWithInput "bash" []  "cat /sys/class/power_supply/BAT0/capacity"
  dat <- runProcessWithInput "bash" []  "date +'%d/%m/%Y'"
  day <- runProcessWithInput "bash" []  "date +'%A'"
  time <- runProcessWithInput "bash" []  "date +'%H-%M-%d-%m-%Y'"
  previousShutDown <- liftIO retriveData
  spawnSelected def{ gs_cellheight = 100,
                     gs_cellwidth = 300 , gs_colorizer = myColorizer ,
                     gs_font = myFont  -- "xft:Consolas:size=30"
                   } ["rgv", dat,day,time,previousShutDown]
showLastShutdown = do
  previousShutDown <- liftIO retriveData
  spawnSelected def{ gs_cellheight = 100,
                     gs_cellwidth = 300 , gs_colorizer = myColorizer ,
                     gs_font = myFont  -- "xft:Consolas:size=30"
                   } [previousShutDown,"LastshutDown "]

-- parseDelimeter :: String -> Char -> [String]
-- parseDelimeter
convertToInt :: String -> [Int]
convertToInt =   (map (read :: String -> Int) ) .  (splitOn "-")

difference :: String -> String -> [Int]
difference xs ys = absMinute $ zipWith (-) ixs iys
  where ixs = convertToInt xs
        iys = convertToInt ys

absMinute [hr,minute,day,mon,year]
  | minute < 0 = [hr-1,60-minute,day,mon,year]
  | otherwise = [hr,minute,day,mon,year]

convertToString :: [Int] -> String
convertToString =   unwords . map show

myTerm "alacritty" = "Alacritty"
myTerm "kitty" = "kitty"

myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , className =? "mpv"            --> doShift "<>"
    , className =? "Spotify"        --> doShift "<>"
    , className =? "Brave-browser"  --> doShift "<>"
    , className =? "Zathura"        --> doShift "<*>"
    , className =? "firefox"        --> doShift "λ="
    , className =? "Emacs"        --> doShift ">>="
    , resource  =? "kdesktop"       --> doIgnore ]
  <+> namedScratchpadManageHook myScratchPads

mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

-- Defining a bunch of layouts, many that I don't use.
-- limitWindows n sets maximum number of windows displayed for layout.
-- mySpacing n sets the gap size around the windows.
tall     = renamed [Replace "tall"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 8
           $ ResizableTall 1 (3/100) (1/2) []
-- magnify  = renamed [Replace "magnify"]
--            $ smartBorders
--            $ windowNavigation
--            $ addTabs shrinkText myTabTheme
--            $ subLayout [] (smartBorders Simplest)
--            $ magnifier
--            $ limitWindows 12
--            $ mySpacing 8
--            $ ResizableTall 1 (3/100) (1/2) []
monocle  = renamed [Replace "monocle"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 20 Full
floats   = renamed [Replace "floats"]
           $ smartBorders
           $ limitWindows 20 simplestFloat
grid     = renamed [Replace "grid"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 8
           $ mkToggle (single MIRROR)
           $ Grid (16/10)
spirals  = renamed [Replace "spirals"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ mySpacing' 8
           $ spiral (6/7)
threeCol = renamed [Replace "threeCol"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 7
           $ ThreeCol 1 (3/100) (1/2)
threeRow = renamed [Replace "threeRow"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 7
           -- Mirror takes a layout and rotates it by 90 degrees.
           -- So we are applying Mirror to the ThreeCol layout.
           $ Mirror
           $ ThreeCol 1 (3/100) (1/2)
tabs     = renamed [Replace "tabs"]
           -- I cannot add spacing to this layout because it will
           -- add spacing between window and tabs which looks bad.
           $ tabbed shrinkText myTabTheme
tallAccordion  = renamed [Replace "tallAccordion"]
           $ Accordion
wideAccordion  = renamed [Replace "wideAccordion"]
           $ Mirror Accordion

-- setting colors for tabs layout and tabs sublayout.
myTabTheme = def { fontName            = myFont
                 , activeColor         = "#46d9ff"
                 , inactiveColor       = "#313846"
                 , activeBorderColor   = "#46d9ff"
                 , inactiveBorderColor = "#282c34"
                 , activeTextColor     = "#282c34"
                 , inactiveTextColor   = "#d0d0d0"
                 }

-- The layout hook
myLayoutHook = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
             where
               myDefaultLayout = withBorder myBorderWidth tall
                                 -- ||| magnify
                                 ||| floats
                                 ||| noBorders monocle
                                 ||| grid
                                 ||| noBorders tabs
                                 ||| spirals
                                 ||| threeCol
                                 ||| threeRow
                                 ||| tallAccordion
                                 ||| wideAccordion

myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
    { swn_font              =   "xft:Cousine:bold:size=70"
    , swn_fade              = 1.0
    , swn_bgcolor           = "#1c1f24"
    , swn_color             = "#ffffff"
    }

myEventHook = docksEventHook <+> handleEventHook def <+> fullscreenEventHook --mempty

myStartupHook :: X ()
myStartupHook = do
  showLastShutdown
  -- spawnOnce "sudo ~/kmonad/nix/result/bin/kmonad ~/kmonad/keymap/tutorial.kbd"
  -- spawnOnce "firefox https://www.youtube.com/results?search_query=rgv"
  -- spawnOnce "systemctl --user stop gammastep.service"
  -- spawnOnce "sudo ~/kmonad/nix/result/bin/kmonad ~/knuth/logitech.kbd &"
  -- spawnOnce "sudo ~/kmonad/nix/result/bin/kmonad ~/knuth/dell.kbd &"
  -- spawn "systemctl --user start start-notifier-watcher.service"
  -- spawnOnce "~/custom-taffybar/result/bin/taffybar &"
  -- spawnOnce "brightnessctl s 1"
  -- spawnOnce "xrandr --output HDMI-1 --auto"
  -- makeGrid2 hdmiList
  setWMName "LG3D"
  -- spawnOnce myEditor


main :: IO ()
main = do
  xmproc0 <- spawnPipe "xmobar -x 0 $HOME/.config/xmobar/.xmobarrc"
    -- xmproc1 <- spawnPipe "xmobar -x 1 $HOME/.config/xmobar/doom-one-xmobarrc"
    -- xmproc2 <- spawnPipe "xmobar -x 2 $HOME/.config/xmobar/doom-one-xmobarrc"
  xmonad  $ docks $ ewmh def
	{
      -- simple stuff
	terminal           = myTerminal,
	focusFollowsMouse  = myFocusFollowsMouse,
	clickJustFocuses   = myClickJustFocuses,
	borderWidth        = myBorderWidth,
	modMask            = myModMask,
	workspaces         = myWorkspaces,
	normalBorderColor  = myNormColor,
	focusedBorderColor = myFocusColor,

      -- key bindings
	-- keys               = myKeys,
	mouseBindings      = myMouseBindings

      -- hooks, layoutshowWName' myShowWNameTheme s$
	,layoutHook         =   myLayoutHook
	, manageHook         = myManageHook <+> manageDocks
	,handleEventHook    =  myEventHook
	, logHook            = extraLogHook $ myLogHook {ppOutput =  hPutStrLn xmproc0}
	,startupHook        = myStartupHook
 }
	   `additionalKeysP` vamKeys
  where extraLogHook =  dynamicLogWithPP . namedScratchpadFilterOutWorkspacePP

myLogHook =  xmobarPP
              -- the following variables beginning with 'pp' are settings for xmobar.
              { -- ppOutput =  >>= hPutStrLn                   -- xmobar on monitor 1
               ppCurrent = myppCurrent
              , ppVisible = myppVisible
              , ppHidden = myppHidden
              , ppHiddenNoWindows = myppHiddenNoWindows
              , ppTitle = myppTitle
              , ppSep = myppSep
              , ppUrgent = myppUrgent
              , ppExtras  = myppExtras
              , ppOrder  = myppOrder
              }
clickable ws = "<action=xdotool key super+"++show i++">"++ws++"</action>"
    where i = fromJust $ M.lookup ws myWorkspaceIndices

myIntersprese = map (\x -> "  " ++ x ++ "  ")
myppSep =   "<fc=" ++ color09 ++ "> <fn=1>|</fn> </fc>"--"<fn=1>    </fn>"                 -- Separator character
myppExtras = [windowCount]                                     -- # of windows current workspace
myppCurrent = xmobarColor color06 "" . wrap
                            ("<box type=Bottom width=2 mb=2 color=" ++ color06 ++ ">") "</box>"
  -- xmobarColor "#000000" "" . wrap "<box type=Bottom width=2 mb=2 color=#c792ea>""</box>"         -- Current workspace
myppHidden =  xmobarColor color05 "" . wrap
                           ("<box type=Top width=2 mt=2 color=" ++ color05 ++ ">") "</box>" . clickable
  --xmobarColor "#000000" "" . wrap "<box type=Top width=2 mt=2 color=#82AAFF>""</box>" --  . clickable -- Hidden workspaces
myppHiddenNoWindows =     xmobarColor color05 ""  . clickable
  --xmobarColor "#000000" "" --  . clickable     -- Hidden workspaces (no windows)
myppTitle =  xmobarColor color02 "" . shorten 40
  --xmobarColor "#000000" "" . shorten 60               -- Title of active window
myppUrgent = xmobarColor color02 "" . wrap "!" "!"
  --xmobarColor "#000000" "" . wrap "!" "!"            -- Urgent workspace
myppVisible =   xmobarColor color06 "" . clickable
  -- xmobarColor "#000000" "" -- . clickable              -- Visible but not current workspace
myppOrder =  \(ws:l:t:ex) ->   [ws,l]++ ex ++[t]                  -- order of things in xmobar
-- +["<fn=3>  \xf179</fn>"]
take12 = take 12
myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                , NS "mocp" spawnMocp findMocp manageMocp
                , NS "spotify" spawnSpotify findSpotify manageSpotify
                ]
  where
    spawnTerm  = myTerminal -- ++ " -n scratchpad"
    findTerm   = className =?   myTerm myTerminal
    manageTerm = customFloating $ W.RationalRect l t w h
               where
                 h = 0.50
                 w = 0.84
                 t = 0.07
                 l = 0.1
    spawnMocp  = myTerminal ++ " -n mocp 'mocp'"
    findMocp   = resource =? "mocp"
    manageMocp = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w
    spawnSpotify  =  "spotify"-- ++ " -n scratchpad"
    findSpotify   = className =? "Spotify"--myTerminal
    manageSpotify = customFloating $ W.RationalRect l t w h
               where
                 h = 0.80
                 w = 0.84
                 t = 0.9 -h
                 l = 0.91 -w
