# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ./m1-support
  ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = false;

  #networking.hostName = "cosmos"; # Define your hostname.
  # Pick only one of the below networking options.

  networking = {
    hostName = "cosmos";
    wireless = {
      enable = true; # Enables wireless support via wpa_supplicant.
      userControlled.enable = true;
      interfaces = [ "wlp1s0f0" ];
      networks.Varikuti.pskRaw =
        "2f5385967f945b489e113af64fb060b86319cf195898249a8cd0189e63078cbd";
    };
  };
  # networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

  nix = {
    settings.auto-optimise-store = true;
    gc = {
      automatic = true;
      dates = "daily";
    };
    #        package = pkgs.nixUnstable;
    extraOptions = ''
      experimental-features = nix-command flakes
      keep-outputs = true
      keep-derivations = true
    '';
  };
  security.sudo.wheelNeedsPassword = false;

  # Set your time zone.
  time.timeZone = "Asia/Kolkata";
  fonts.fonts = with pkgs; [font-awesome source-code-pro ubuntu_font_family mononoki fira-code];
 #fonts.fontconfig = {
 #            defaultFonts = {
# 		monospace = [ "Fira Code"  ];
#             };
# };

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
    #   useXkbConfig = true; # use xkbOptions in tty.
  };
  #hardware.asahi.pkgsSystem = "x86_64-linux";
  # Enable the X11 windowing system.
  # services.xserver.enable = true;

  # Configure keymap in X11

  services.xserver = {
    enable = true;
    layout = "us";
    xkbOptions = "caps:escape";
    windowManager.xmonad = {
      enable = true;
      enableContribAndExtras = true;
      config = ./xmonad.hs;
    extraPackages = p: [ p.split];  
  };
    desktopManager.wallpaper.mode = "fill";
    displayManager = {
      defaultSession = "none+xmonad";
      autoLogin = {
        enable = true;
        user = "vamshi";
      };
    };
      xrandrHeads =
      [
          {
              output =  "None-1";
#              monitorConfig = "Option \"Rotate\" \"left\"";
          }

      ];
    #};

  };
  #  services.xserver.windowManager.xmonad.enable = true;
  #services.xserver.windowManager.xmonad.config = ./xmonad.hs;
  #services.xserver.windowManager.xmonad.enableContribAndExtras = true;
  #   services.xserver.xkbOptions = #{
  #   "eurosign:e";
  #     "caps:escape"; # map caps to escape.
  #   };

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;
  hardware.bluetooth.enable = true;
  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.vamshi = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
    shell = pkgs.fish;
    packages = with pkgs; [
 pavucontrol     
 playerctl
 firefox
      git
      dmenu
      alacritty
      #     thunderbird
    ];
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  # environment.systemPackages = with pkgs; [
  #   vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
  #   wget
  # ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.11"; # Did you read the comment?

}

