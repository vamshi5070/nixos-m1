{ config, pkgs, ... }: {
	programs.kitty = {
		enable = true;
		font = {
			name =	"Fira Code";
			size = 12;
		};
		theme = "Dracula";
	};
}
