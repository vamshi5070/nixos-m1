{
  description = "Home manager flake";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    neovim-nightly-overlay.url = "github:nix-community/neovim-nightly-overlay";
    emacs-overlay.url = "github:nix-community/emacs-overlay";
    nur = {
      url = "github:nix-community/NUR";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs = inputs@{ self, nixpkgs, ... }:

    let
      system = "aarch64-linux";

      pkgs = import nixpkgs {

        inherit system;

        config = { allowUnfree = true; };
      };

      lib = nixpkgs.lib;
      nurNoPkgs = import inputs.nur {
        pkgs = null;
        nurpkgs = pkgs;
      };

    in {
      homeConfigurations = {
        cosmos = inputs.home-manager.lib.homeManagerConfiguration {
          pkgs = import inputs.nixpkgs {
            system = "aarch64-linux";
            config.allowUnfree = true;
          };
          #         system = "aarch64-linux";
          #         homeDirectory = "/home/vamshi";
          #         username = "vamshi";
          #         configuration = { config, lib, pkgs, ... }: {
          #            nixpkgs.overlays = with inputs;
          #              [
          #                emacs-overlay.overlay
          #  nur.overlay
          #  kmonad.overlay
          #neovim-nightly-overlay.overlay
          #              ];
          #            nixpkgs.config = { allowUnfree = true; };
          # This value determines the Home Manager release that your
          # configuration is compatible with. This helps avoid breakage
          # when a new Home Manager release introduces backwards
          # incompatible changes.
          #
          # You can update Home Manager without changing this value. See
          # the Home Manager release notes for a list of state version
          # changes in each release.
          # home.stateVersion = "21.05";
          # home.keyboard = null;
          # Let Home Manager install and manage itself.
          #         programs = { home-manager.enable = true; };
          #programs.kitty.enable = true;
          modules = [
            ./picom
            #./emacs
            ./wallpapers
            #		./nyxt
            ./htop
            ./alacritty
            ./git
            #              ./zathura
            ./fish
            ./nix
            ./kakoune
            #./xmobar
            #              ./pcmanfm
            #./kakoune
            ./direnv
            ./kitty
            #./firefox
            # ./neovim 
./starship
            #              ./xmobar
            {
              nixpkgs.overlays = with inputs; [
                emacs-overlay.overlay
                nur.overlay
                #  kmonad.overlay
                neovim-nightly-overlay.overlay
              ];

              programs.home-manager.enable = true;
              home = {
                username = "vamshi";
                homeDirectory = "/home/vamshi";
                stateVersion = "22.11";
              };
            }
          ];
        };
      };
    };
}
