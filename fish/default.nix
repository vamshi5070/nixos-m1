{ pkgs, config, lib, ... }: {
  programs.fish = {
    enable = true;
    interactiveShellInit = ''
      					### AUTOCOMPLETE AND HIGHLIGHT COLORS ###
      					set PATH $PATH /nix/store/nlgwv0h3pqrng56gjfzi4v5bpalwjn2y-system-path/bin
      					set fish_color_normal brcyan
      					set fish_color_autosuggestion '#7d7d7d'
      					set fish_color_command brcyan
      					set fish_color_error '#ff6c6b'
                                              set fish_color_param brcyan '';
    shellInit =
      "\n            export PATH=\"$HOME/.emacs.d/bin:$PATH\"\n            set fish_greeting";
    shellAliases = {
      conf = "sudo nvim /etc/nixos/default.nix";
      # homeConf =  "nvim ~/.config/nixpkgs/home.nix";
      up = "sudo nixos-rebuild switch"; # && notify-send -u low \"Successfully updated\" ";
      addRoot = "cp -r /etc/nixos/* ~/nixos-m1/root/";
        
      upBoot = "sudo nixos-rebuild boot --install-bootloader";
      del = "sudo nix-collect-garbage -d";
      # server = "php -S localhost:4000";
      # bright = "systemctl --user stop redshift.service";
      # dark = "systemctl --user restart redshift.service";
      update = "sudo nixos-rebuild switch ; cp -r /etc/nixos/* ~/nixos-m1/root/   ";
      wifi = "systemctl start wpa_supplicant-wlp2s0.service";
      c = "clear";
      e = "exit";
      homeUp = ''home-manager switch --flake "/home/vamshi/nixos-m1#cosmos"'';
      # ls = "exa";
      # ll = "exa -l -g --icons";
    };
  };
  # xdg.configFile."fish/functions/fish_prompt.fish".text = customPlugins.prompt;
}
