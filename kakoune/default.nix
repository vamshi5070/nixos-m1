{ config, pkgs, ... }: {
  programs.kakoune = {
    enable = true;
    plugins = with pkgs.kakounePlugins; [
   #     kakoune-easymotion
#   quickscope-kak
powerline-kak
    ];
    extraConfig = ''
    	powerline-start
    '';
    config = {
	    colorScheme = "gruvbox-dark";
	    ui =  {
    	    	statusLine = "top";
	    };
	    numberLines =
	    {
    	    	enable = true;
    	    	highlightCursor = true;
    	    	relative = true;
	    };
    };
  };
}
