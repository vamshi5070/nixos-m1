{ pkgs, ... }: {
  services.picom = {
    enable = true;
    package = pkgs.picom.overrideAttrs (o: {
      src = pkgs.fetchFromGitHub {
        repo = "picom";
        owner = "ibhagwan";
        rev = "44b4970f70d6b23759a61a2b94d9bfb4351b41b1";
        sha256 = "0iff4bwpc00xbjad0m000midslgx12aihs33mdvfckr75r114ylh";
      };
    });
    activeOpacity = 1.0;
    backend = "glx";
    #    experimentalBackends = true;
    inactiveOpacity = 0.8;
    settings = {
      blur = {
        method = "gaussian";
        size = 10;
        deviation = 5.0;
      };
      corner-radius = 16;
      round-borders = 16;
      blur-method = "dual_kawase";
      blur-strength = "10";
      xinerama-shadow-crop = true;
      vSync = true;
    };
    fade = true;
    fadeDelta = 5;
    #    opacityRule = [ "100:name *= 'i3lock'" ];
    shadow = true;
    shadowOpacity = 0.75;
  };
}
